import ProjectPage from "./ProjectPage";
import WriteUpPage from "./WriteUpPage";
import MarkdownPage from "./MarkdownPage";
import AboutPage from "./AboutPage";


export {
  ProjectPage,
  WriteUpPage,
  MarkdownPage,
  AboutPage
}
