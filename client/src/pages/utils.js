export const isOnProjectPage = (location) => {
  return location.pathname.match(/^\/$/);
}
export const isOnMarkdownPage = (location) => {
  return location.pathname.match(/^\/writeups\/\w+$/);
}
export const isOnWriteUpPage = (location) => {
  return location.pathname.match(/^\/writeups$/);
}
