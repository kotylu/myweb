import easyctf from "./thm-easyctf.md";
import networkServices2 from "./thm-network-services-2.md"
import rootme from "./thm-rootme.md"

export {
  easyctf,
  networkServices2,
  rootme,
}
