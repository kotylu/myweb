# Try Hack Me | easyctf

## PROBING

open ports:
* 21 ftp
* 80 apache 2.4.18
* 2222 OpenSSH 7.2p2

found SSH vulnerability
CVE-2016-6120 | [exploit-db](https://www.exploit-db.com/exploits/40136)


on port 80 is APACHE default welcome page
-- had connection problems restart machines
-- thats why different address


ran gobuster dir
* /simple 
* /robots.txt 

## LOOKING FOR VULNS

* SSH - CVE-2016-6120 | [exploit-db](https://www.exploit-db.com/exploits/40136)
* ftp:/simple- leads to source content management tool | [cms-made-simple](http://www.cmsmadesimple.org/) version 2.2.8 
* ftp:/robots.txt found folder openemr - leads nowhere

* CMS - XSS (cross-site scripting) quit a few of them
* CMS - sqlinjection *thats whats supposed to be used*

## EXPLOIT

via exploit-db i found sqlinjection vulnerability but exploit-db was nice enough to provide script that can exploit that
due to problematic connection i am having at the moment exploit script could not finish

I got salt, user (**mitch**), and mail (**admin@admin.com**) but connection timed out before i got password
so I am gonna use password from room write-up ([floreaiulianpfa](https://floreaiulianpfa.com/simple-ctf/))

**login: mitch:secret
after connecting via ssh
ran sudo -l that told me i can use vim as root

so fire up vim and run command !bash that puts you into bash but with user that started vim and we started it as root with sudo so we are now in root bash


