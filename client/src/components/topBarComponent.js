import React, { Fragment, useCallback, useEffect, useRef } from "react";
import { Link, useLocation } from "react-router-dom";
import { isOnMarkdownPage, isOnProjectPage, isOnWriteUpPage } from "../pages/utils";
import { appStateStore, glProjectStore } from "../Stores";

const TopBar = () => {
  const htmlInput = useRef(null);

  const appState = appStateStore.useState();
  const gitlab = glProjectStore.useState();
  const location = useLocation();

  //handle popping of search bar
  const searchHandler = useCallback(() => {
    if (!gitlab.searchTerm) {
      appStateStore.update(store => {
        store.isSearchOpen = !appState.isSearchOpen
      });
    }
  }, [appState.isSearchOpen, gitlab.searchTerm]);

  const secondaryButton = () => {
    const searchSVGStyle = () => {
      if (appState.isSearchOpen) {
        return {color: "#f47802", width: "2rem"}
      }
      else if (isOnMarkdownPage(location) || isOnWriteUpPage(location)) {
        return {display: "none"}
      }
      else {
        return {width: "2rem"}
      }

    } 
    return (
      <>
        <input
          ref={htmlInput}
          type="text"
          value={gitlab.searchTerm}
          onChange={(event) => {
            glProjectStore.update((store) => {
              store.searchTerm = event.target.value;
            })
          }}
          className={!!gitlab.searchTerm || appState.isSearchOpen ? "searchInput" : "disabled"}
        />
        <svg
          onClick={searchHandler}
          style={searchSVGStyle()}
          aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" className="svg-inline--fa fa-search fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path></svg>
        <Link to="/writeups" style={isOnProjectPage(location) || isOnWriteUpPage(location) ? {display: "none"} : {}}>
          <svg style={{width: "1.5rem"}} aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" className="svg-inline--fa fa-chevron-left fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path></svg>
        </Link>
      </>
    )
  };
  

  //set automatic focus on input when search is clicked
  useEffect(() => {
    if (appState.isSearchOpen) {
      htmlInput.current.focus();
    }
  }, [appState.isSearchOpen]);

  return (
    <div className="top-bar">
      <Link to="/">
        <div className="logo-container">
          <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 475.28 111.57" id="svg16"> <g aria-label="&lt;kotylu /&gt;" id="text12" className="cls-1"> <path d="m 6.5452192,57.821644 v -8.70522 L 45.968858,23.200885 v 14.908939 l -23.814279,15.009 v 0.70042 l 23.814279,15.008999 v 15.009 z" id="path24" /> <path d="m 63.183181,9.5927249 h 14.80888 V 53.318944 h 0.60036 l 6.5039,-9.70582 10.506299,-12.5075 h 16.40984 l -17.5105,20.11206 19.5117,32.419439 H 96.4031 l -11.807079,-22.213319 -6.60396,7.404439 v 14.80888 h -14.80888 z" id="path26" /> <path d="m 138.32597,84.837843 q -5.80348,0 -10.40624,-1.90114 -4.5027,-1.90114 -7.70462,-5.5033 -3.10186,-3.60216 -4.80288,-8.70522 -1.70102,-5.103059 -1.70102,-11.406839 0,-6.30378 1.70102,-11.40684 1.70102,-5.10306 4.80288,-8.60516 3.20192,-3.60216 7.70462,-5.5033 4.60276,-1.90114 10.40624,-1.90114 5.80348,0 10.30618,1.90114 4.60276,1.90114 7.70462,5.5033 3.20192,3.5021 4.90294,8.60516 1.70102,5.10306 1.70102,11.40684 0,6.30378 -1.70102,11.406839 -1.70102,5.10306 -4.90294,8.70522 -3.10186,3.60216 -7.70462,5.5033 -4.5027,1.90114 -10.30618,1.90114 z m 0,-11.70702 q 4.40264,0 6.80408,-2.70162 2.40144,-2.70162 2.40144,-7.704619 v -10.70642 q 0,-5.003 -2.40144,-7.70462 -2.40144,-2.70162 -6.80408,-2.70162 -4.40264,0 -6.80408,2.70162 -2.40144,2.70162 -2.40144,7.70462 v 10.70642 q 0,5.002999 2.40144,7.704619 2.40144,2.70162 6.80408,2.70162 z" id="path28" /> <path d="m 191.35423,83.637123 q -7.70462,0 -11.70702,-3.90234 -3.90234,-3.90234 -3.90234,-11.40684 V 42.612524 h -7.40444 v -11.5069 h 3.70222 q 3.0018,0 4.10246,-1.40084 1.10066,-1.5009 1.10066,-4.202519 v -8.60516 h 13.30798 v 14.208519 h 10.40624 v 11.5069 h -10.40624 v 29.517699 h 9.60576 v 11.5069 z" id="path30" /> <path d="m 243.38537,31.105624 h 13.80828 l -20.21212,60.636359 q -2.0012,6.10366 -5.70342,9.005397 -3.60216,2.90174 -10.40624,2.90174 h -9.20552 V 92.142223 h 9.90594 l 1.80108,-5.80348 -18.41104,-55.233119 h 14.70882 l 7.70462,24.5147 3.80228,15.209119 h 0.60036 l 3.90234,-15.209119 z" id="path32" /> <path d="m 279.40681,83.637123 q -7.60456,0 -11.20672,-3.70222 -3.5021,-3.70222 -3.5021,-10.70642 V 9.5927249 h 14.80888 V 72.130223 h 6.60396 v 11.5069 z" id="path34" /> <path d="m 326.03472,74.831843 h -0.5003 q -0.70042,2.0012 -1.90114,3.80228 -1.10066,1.80108 -2.90174,3.20192 -1.80108,1.40084 -4.30258,2.20132 -2.40144,0.80048 -5.60336,0.80048 -8.10486,0 -12.30738,-5.30318 -4.10246,-5.40324 -4.10246,-15.409239 v -33.0198 h 14.80888 v 31.71902 q 0,4.802879 1.70102,7.404439 1.70102,2.60156 5.80348,2.60156 1.70102,0 3.40204,-0.5003 1.70102,-0.5003 3.0018,-1.40084 1.30078,-1.0006 2.10126,-2.40144 0.80048,-1.400839 0.80048,-3.301979 v -34.12046 h 14.80888 v 52.531499 h -14.80888 z" id="path36" /> <path d="M 371.66178,98.746183 401.47966,9.5927249 h 12.20732 L 383.8691,98.746183 Z" id="path38" /> <path d="m 427.69528,68.828243 23.81428,-15.008999 v -0.70042 l -23.81428,-15.009 V 23.200885 l 39.42364,25.815479 v 8.70522 l -39.42364,26.115659 z" id="path40" /> </g> </svg> 
        </div>
      </Link>
      <div className="search-container">
        {secondaryButton() }
      </div>
    </div>
  )
}

export default TopBar;