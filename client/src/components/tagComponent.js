import React from "react";

const TagComponent = ({ tag }) => (
  <div className="tag-container">
    <p>{tag}</p>
  </div>
);

export default TagComponent;