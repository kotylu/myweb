const fetch = require("node-fetch");

const gitlabProjects = async () => {
  const res = await fetch("https://gitlab.com/api/v4/projects?membership=true", {
    headers: {
      "content-type": "application/json",
      "PRIVATE-TOKEN": process.env.GITLAB_TOKEN,
    },
  });
  return res.json();
};

const gitlabProjectsPrety = async () => {
  const raw = await gitlabProjects();
  return raw
    .filter((project) => (project.visibility === "public"))
    .map((project, ix) => ({
      _id: ix,
      icon: project.avatar_url,
      name: project.name,
      description: project.description === "" ? "No description provided" : project.description,
      link: project.web_url,
      tags: project.tag_list,
    }));
};

module.exports = {
  gitlabProjectsPrety,
};
