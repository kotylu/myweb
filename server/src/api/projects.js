const { Router } = require("express");
const { gitlabProjectsPrety } = require("./outerApi");

const router = Router();

router.get("/", async (req, res) => {
  const projects = await gitlabProjectsPrety();
  res.json(projects);
});

module.exports = router;
