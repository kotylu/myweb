const express = require("express");
const helmet = require("helmet");
const morgan = require("morgan");
const cors = require("cors");
const dotenv = require("dotenv");
const { notFound, errorHandler } = require("./middlewares");
const projects = require("./api/projects");

dotenv.config();

const app = express();
app.use(morgan("common"));
app.use(helmet());
app.use(cors());

app.get("/", (req, res) => {
  res.json({
    message: "Hello World",
  });
});

app.use("/gitlab/projects", projects);

app.use(notFound);
app.use(errorHandler);

const port = process.env.PORT || 2001;
app.listen(port, () => {
  console.log("linstening...");
});
